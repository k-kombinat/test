import { BrowserRouter, Route, Switch } from "react-router-dom";
import React from "react";
import DefaultTemplate from "./components/Template";
import ViewItemPage, { GalleryItem } from "./pages/ViewItemPage";
import ViewItemPagePhotoSwipe from "./pages/ViewItemPagePhotoSwipe";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact
          path="/"
          children={
            <DefaultTemplate>
              <ViewItemPage />
            </DefaultTemplate>
          }
        />

        <Route path="/img/:key" children={<GalleryItem />} />

        <Route
          exact
          path="/photoswipe"
          children={
            <DefaultTemplate>
              <ViewItemPagePhotoSwipe />
            </DefaultTemplate>
          }
        />

        <Route children={<DefaultTemplate>404</DefaultTemplate>} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
