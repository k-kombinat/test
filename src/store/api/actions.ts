import axios from "axios";

export const FETCH_API_DATA_BEGIN = "FETCH_API_DATA_BEGIN";
export const FETCH_API_DATA_SUCCESS = "FETCH_API_DATA_SUCCESS";
export const FETCH_API_DATA_FAILURE = "FETCH_API_DATA_FAILURE";

export const fetchApiDataBegin = () => ({
  type: FETCH_API_DATA_BEGIN
});

export const fetchApiDataSuccess = items => ({
  type: FETCH_API_DATA_SUCCESS,
  payload: { items }
});

export const fetchApiDataFailure = error => ({
  type: FETCH_API_DATA_FAILURE,
  payload: { error }
});

interface IImageItems {
  title: string;
  src: string;
  uri: string;
  thumbnail: string;
  w: number;
  h: number;
}

export function fetchApiData(apiCall) {
  return dispatch => {
    dispatch(fetchApiDataBegin());

    return axios
      .get(apiCall)
      .then(res => {
        const data = res.data;
        const images = data.images;

        let img;
        let out = images.map((image: IImageItems) => {
          img = {};
          img.title = data.title;
          img.src = "https://" + image.uri.replace("$", "$_27.jpg");
          img.thumbnail = "https://" + image.uri.replace("$", "$_2.jpg");
          img.w = window.innerWidth < 800 ? 600 : 1200;
          img.h = window.innerWidth < 800 ? 450 : 900;
          return img;
        });
        dispatch(fetchApiDataSuccess(out));
        return out;
      })
      .catch(error => dispatch(fetchApiDataFailure(error)));
  };
}
