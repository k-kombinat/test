import {
  FETCH_API_DATA_BEGIN,
  FETCH_API_DATA_FAILURE,
  FETCH_API_DATA_SUCCESS
} from "./actions";

const initialState = {
  items: [],
  loading: false,
  error: null
};

export default function getApiReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_API_DATA_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_API_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload.items
      };

    case FETCH_API_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };

    default:
      return state;
  }
}
