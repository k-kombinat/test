import { combineReducers } from "redux";
import api from "./reducer";

export default combineReducers({
  api
});
