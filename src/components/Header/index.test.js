import React from "react";
import { shallow } from "enzyme";
import Header from "./index";
import styles from "./style.module.scss";

it("renders logo", () => {
  const wrapper = shallow(<Header />);
  const image = (
    <img
      className={styles.logo}
      role="presentation"
      alt="eBay-Logo"
      src="https://ir.ebaystatic.com/rs/v/fxxj3ttftm5ltcqnto1o4baovyl.png"
    />
  );
  expect(wrapper.contains(image)).toEqual(true);
});
