import React from "react";
import { Link } from "react-router-dom";
import styles from "./style.module.scss";

export default function Header() {
  return (
    <header className={styles.header}>
      <Link to="/" title="Home" id="nav-logo-home">
        <img
          className={styles.logo}
          role="presentation"
          alt="eBay-Logo"
          src="https://ir.ebaystatic.com/rs/v/fxxj3ttftm5ltcqnto1o4baovyl.png"
        />
      </Link>
    </header>
  );
}
