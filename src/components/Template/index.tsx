import React from "react";
import { Col, Container, Row } from "react-grid-system";
import Header from "../Header";
import Footer from "../Footer";

export default function DefaultTemplate(props) {
  return (
    <Container>
      <Row>
        <Col>
          <Header />
        </Col>
      </Row>

      <Row>
        <Col>
          <main style={{ minHeight: "80vh" }}>{props.children}</main>
        </Col>
      </Row>

      <Row>
        <Col>
          <Footer />
        </Col>
      </Row>
    </Container>
  );
}
