import React from "react";
import { shallow } from "enzyme";
import Footer from "./index";
import styles from "./style.module.scss";

it("renders footer", () => {
  const wrapper = shallow(<Footer />);
  expect(wrapper.contains("Photoswipe gallery")).toEqual(true);
});
