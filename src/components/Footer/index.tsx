import React from "react";
import { Link } from "react-router-dom";
import styles from "./style.module.scss";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <Link to={"/photoswipe"}>Photoswipe</Link>
      &nbsp;
      <Link to={"/"}>Home</Link>
    </footer>
  );
}
