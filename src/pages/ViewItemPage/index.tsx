import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "react-grid-system";
import { fetchApiData } from "../../store/api/actions";
import styles from "./style.module.scss";
import { Link, useHistory, useParams } from "react-router-dom";

export function GalleryItem() {
  const { items } = useSelector(state => state.api);
  let history = useHistory();
  let { key } = useParams();
  let image = items[parseInt(key, 10)];

  if (!image) return null;

  let goBack = e => {
    e.stopPropagation();
    history.goBack();
  };

  return (
    <div className={styles.modal}>
      <div className={styles.close} onClick={goBack}></div>
      <div className={styles.content}>
        <h1>{image.title}</h1>
        <img src={image.src} alt={""} />
      </div>
    </div>
  );
}

export default function ViewItemPage() {
  const dispatch = useDispatch();
  const { items, loading, error } = useSelector(state => state.api);

  useEffect(() => {
    if (items.length === 0) {
      dispatch(
        fetchApiData("https://www.mobile.de/hiring-challenge.json")
      ).catch(error => {
        console.log(error.response);
      });
    }
  });

  const Thumbnails = () => {
    if (items.length > 0) {
      return items.map((image, i) => {
        return (
          <Col key={i} md={4} className={styles.item}>
            <Link
              to={{
                pathname: `/img/${i}`
              }}
            >
              <img
                src={image.thumbnail}
                alt={""}
                className={styles.thumbnail}
              />
            </Link>
          </Col>
        );
      });
    } else {
      return (
        <Col>
          <span className="no-results">No results</span>
        </Col>
      );
    }
  };

  if (error) {
    return error;
  }

  return (
    <>
      <h1>Car photos</h1>

      {loading ? (
        <Row>
          <Col>
            <span className="loading">Loading</span>
          </Col>
        </Row>
      ) : (
        <>
          <h2>Gallery routing</h2>
          <Row>
            <Thumbnails />
          </Row>
        </>
      )}
    </>
  );
}
