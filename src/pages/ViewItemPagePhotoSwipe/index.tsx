import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "react-grid-system";
import { PhotoSwipe } from "react-photoswipe";
import { fetchApiData } from "../../store/api/actions";
import styles from "./style.module.scss";
import "react-photoswipe/lib/photoswipe.css";

// Define interfaces
interface IModalOptions {
  index: Number;
}

export default function ViewItemPagePhotoSwipe() {
  const dispatch = useDispatch();
  let { items, loading, error } = useSelector(state => state.api);
  const [isOpen, setModalState] = useState<boolean>(false);
  const [modalOptions, setModalOptions] = useState<IModalOptions>({
    index: 0
  });

  useEffect(() => {
    if (items.length === 0) {
      dispatch(
        fetchApiData("https://www.mobile.de/hiring-challenge.json")
      ).catch(error => {
        console.log(error.response);
      });
    }
  });

  const modalOpenHandler = e => {
    setModalState(true);
    setModalOptions({ index: parseInt(e.currentTarget.dataset.id, 10) });
  };

  const Thumbnails = () => {
    if (items.length > 0) {
      return items.map((image, i) => {
        return (
          <Col
            key={i}
            md={4}
            data-id={i}
            onClick={modalOpenHandler}
            className={styles.item}
          >
            <img src={image.thumbnail} alt={""} className={styles.thumbnail} />
          </Col>
        );
      });
    } else {
      return (
        <Col>
          <span className="no-results">No results</span>
        </Col>
      );
    }
  };

  if (error) {
    return error;
  }

  return (
    <>
      <h1>Car photos</h1>

      {loading ? (
        <Row>
          <Col>
            <span className="loading">Loading</span>
          </Col>
        </Row>
      ) : (
        <>
          <h2>PhotoSwipe</h2>
          <Row>
            <Thumbnails />
            <PhotoSwipe
              isOpen={isOpen}
              items={items}
              options={modalOptions}
              onClose={() => {
                setModalState(false);
              }}
            />
          </Row>
        </>
      )}
    </>
  );
}
