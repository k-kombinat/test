import React from "react";
import Component from "./index";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import { mount } from "enzyme";
import axios from "axios";

const mockStore = configureMockStore([thunk]);

let store;
let component;

let Mount = store => {
  return mount(
    <Provider store={store}>
      <Component />
    </Provider>
  );
};

describe("ViewItemPage without results", () => {
  beforeEach(() => {
    store = mockStore({
      api: {
        items: [],
        loading: false,
        error: null
      }
    });
    component = Mount(store);
  });

  it("should add to count and display the correct # of counts", () => {
    expect(component.find("h1").text()).toEqual("Car photos");
  });

  it("should have no results", () => {
    expect(component.find(".no-results").text()).toEqual("No results");
  });
});

describe("ViewItemPage loading", () => {
  beforeEach(() => {
    store = mockStore({
      api: {
        items: [],
        loading: true,
        error: null
      }
    });

    component = Mount(store);
  });

  it("should add to count and display the correct # of counts", () => {
    expect(component.find("h1").text()).toEqual("Car photos");
  });

  it("should have no results", () => {
    expect(component.find(".loading").text()).toEqual("Loading");
  });
});

describe("ViewItemPage show items", () => {
  beforeEach(() => {
    store = mockStore({
      api: {
        items: [
          {
            title: "Test",
            src:
              "https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_27.jpg",
            thumbnail:
              "https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_2.jpg",
            w: 1200,
            h: 900
          }
        ],
        loading: false,
        error: null
      }
    });
    component = Mount(store);
  });

  it("should add to count and display the correct # of counts", () => {
    expect(component.find("h1").text()).toEqual("Car photos");
  });

  it("should show item", () => {
    expect(component.find(".thumbnail").length).toBe(1);
  });
});

// Check if we get results from api
// Check result count > 0
it("get results from item api call", async () => {
  const fetch = await axios("https://www.mobile.de/hiring-challenge.json");
  const result = fetch.data;
  expect("images" in result && result.images.length).not.toEqual(0);
});
