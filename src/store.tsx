import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
// Shared reducer between client and server
import reducer from "./store/api";

const configureStore = () => {
  const preloadedState = {};

  return createStore(reducer, preloadedState, applyMiddleware(thunk));
};

export default configureStore;
